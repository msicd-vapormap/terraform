# Déploiement de l'infrastructure as Code Terraform à partir d'un poste local

__Par__ : FETCHEPING FETCHEPING Rossif Borel  
__Email__ : rossifetcheping@outlook.fr  
__École__ : IMT Atlantique  
__Formation__ : Mastère spécialisé Infrastructures Cloud et DevOps  
__Année académique__ : 2022-2023  
__UE__ : Projet fil rouge  
__Dépôt Gitlab__ : <https://gitlab.imt-atlantique.fr/vapormap-cicd/terraform/>  
__Pages Gitlab__ : <https://vapormap-cicd.gitlab-pages.imt-atlantique.fr/terraform>  
__Date__ : 27 Février 2023  

## Prérequis

* Un compte Openstack avec un quota suffisant de ressources  
* Une paire de clé SSH existante sur Openstack  
* Un compte sur une instance Gitlab  
* Un token d'accès à l'api du projet avec des droits "maintainer" pour assurer la gestion du TF State par le Backend de Gitlab  
* Un environnement Terraform  

## Configurer l'environnement local

* Récupérer le dépôt  

```sh
cd $HOME
git clone git@gitlab.imt-atlantique.fr:vapormap-cicd/terraform.git vapormap/terraform
```

* Récupérer le fichier Openstack RC sur horizon et mettre dans le répertoire du projet  

* Initialiser la configuration d'Openstack  

```sh
cd $HOME/vapormap/terraform
source os-openrc.sh
```

* Initialiser la configuration du backend Gitlab State  

```sh
export PROJECT_ID="2012"
export TF_USERNAME="YOUR_USERNAME" # Valeur à modifier
export TF_PASSWORD="glpat-3HszY3mmzV2oHiFWqUob"
export GITLAB_API_V4_URL="https://gitlab.imt-atlantique.fr/api/v4"
export TF_STATE_NAME="vapormap_tf_state"
export TF_ADDRESS="${GITLAB_API_V4_URL}/projects/${PROJECT_ID}/terraform/state/${TF_STATE_NAME}"
```

> Remarque : Remplacer YOUR_USERNAME par votre nom d'utilisateur Gitlab. Sauf changement, les autres valeurs par défauts sont valables pour ce projet.  

* Initialiser Terraform avec la configuration du backend Gitlab State  

```sh
terraform init -reconfigure \
  -backend-config=address=${TF_ADDRESS} \
  -backend-config=lock_address=${TF_ADDRESS}/lock \
  -backend-config=unlock_address=${TF_ADDRESS}/lock \
  -backend-config=username=${TF_USERNAME} \
  -backend-config=password=${TF_PASSWORD} \
  -backend-config=lock_method=POST \
  -backend-config=unlock_method=DELETE \
  -backend-config=retry_wait_min=5
```

## Configurer l'infrastructure

* Modifier le fichier __main.auto.tfvars__ pour personnaliser les ressources de l'infrastructure à provisionner sur Openstack. Les valeurs existances sont fiables.  

* Modifier le fichier __keypair.auto.tfvars__ pour renseigner le nom de la paire de clé sur Openstack et le chemin de votre clé privée sur votre poste.  

> Remarque: Un repertoire __keys/__ est prévu pour garantir la sécurité de la clé si jamais vous souhaitez l'avoir à proximité dans le projet. Tous les fichiers de ce dossier sont systématiquement ignorés lors des commits.  

## Déployer l'infrastructure

* Valider le code Terraform  

```sh
terraform validate
```

* Plannifier l'infrastructure et créer le TF State  

```sh
terraform plan
```

* Appliquer de la configuration et déployer de l'infrastructure  

```sh
terraform apply
```

## Résultats

Suite au déploiement, l'inventaire de l'infrastructure est généré dans le repertoire __inventory/__:  

* le fichier __hosts.ini__ contient l'inventaire de l'infrastructure qui sera exploité par Ansible  
* le fichier __public_ip.ini__ contient les noms et adresses IP flottantes associées aux instances Bastion et manager  
* le fichier __private_ip.ini__ contient les noms et adresse IP privées de tous les noeuds  

> Le TF State est systématiquement mis à jour sur le Backend du projet dans Gitlab  
