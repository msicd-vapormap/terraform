# Infrastructure as Code Terraform

__Par__ : FETCHEPING FETCHEPING Rossif Borel  
__Email__ : rossifetcheping@outlook.fr  
__École__ : IMT Atlantique  
__Formation__ : Mastère spécialisé Infrastructures Cloud et DevOps  
__Année académique__ : 2022-2023  
__UE__ : Projet fil rouge  
__Dépôt Gitlab__ : <https://gitlab.imt-atlantique.fr/vapormap-cicd/terraform/>  
__Pages Gitlab__ : <https://vapormap-cicd.gitlab-pages.imt-atlantique.fr/terraform>  
__Date__ : 27 Février 2023  

## Description

Ce projet permet de déployer une infrastructure Cloud sur Openstack grâce à Terraform.  

Cette infrastructure est constituée d'un réseau, un sous-réseau, un routeur, des groupes de sécurité, des IP flottantes, une instance bastion et trois noeuds.  

Le code d'infrastructure est organisé en modules réutilisable.  

Les ressources à provisionner sont variabilisées pour permettre la personnalisation de l'infrastructure.  

Le TF State de l'infrastructure est stocké et géré dans le Backend Gitlab State du projet  

## Caractéristiques de l'infrastructure

* la bastion et les trois noeuds dont un manager et deux workers communiquent dans le réseau privé en full TCP et UDP.  
* la bastion administre et configure les noeuds de l'infrastructure à partir d'une machine hôte extérieure ou d'une chaine de CD.  
* le manager assure la communication avec l'extérieur et distribue le trafic vers les workers.  
* toutes les instances partagent un fichier etc/hosts commum constitué des noms et adresses IP privées de tous les noeuds.  
* la bastion a un accès en SSH à tous les noeuds.  
* la bastion est accessible en SSH à partir d'internet.  
* le manager est accessible en HTTP à partir d'internet.  

## Utilisation du dépôt pour déployer l'infrastructure

Deux modes opératoires sont disponibles:  

* [Déploiement depuis un poste local](./local.md)  
* [Déploiement depuis un pipeline Gitlab](./pipeline.md)  
