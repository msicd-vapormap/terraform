external_network      = "external"     # Nom du réseau externe

int_network = {
  name = "vap_int_network"
  subnet = "vap_int_subnet"
  cidr = "172.16.102.0/24"
  dns = ["192.44.75.10", "192.108.115.2"]
  router = "vap_int_router"
}

int_secgroup = {
  name     = "vap_sg_node"
  description = "Nom du groupe de sécurité du réseau interne"
}

int_secgroup_rules    = [
  { "source" = "172.16.102.0/24", "protocol" = "tcp", "port" = 0 },
  { "source" = "172.16.102.0/24", "protocol" = "udp", "port" = 0 },
]

bastion_secgroup = {
  name     = "vap_sg_bastion"
  description = "Groupe de sécurité de l'instance bastion"
}

bastion_secgroup_rules    = [
  { "source" = "0.0.0.0/0", "protocol" = "tcp", "port" = 22 },
]

app_secgroup = {
  name     = "vap_sg_manager"
  description = "Nom du groupe de sécurité du noeud manager qui sert l'app"
}

app_secgroup_rules    = [
  { "source" = "0.0.0.0/0", "protocol" = "tcp", "port" = 80 },
  { "source" = "0.0.0.0/0", "protocol" = "tcp", "port" = 9200 },
  { "source" = "0.0.0.0/0", "protocol" = "tcp", "port" = 5601 },
  { "source" = "0.0.0.0/0", "protocol" = "tcp", "port" = 8000 },
  { "source" = "0.0.0.0/0", "protocol" = "tcp", "port" = 8001 }
]

bastion               = {
  flavor              = "m1.medium"       # Nom du gabarit de l'instance bastion
  name                = "vap_bastion"
  image               = "ubuntu-22.04"   # Nom de l'image OS de bastion
  user                = "ubuntu"
  ssh_port            = 22
  role                = ["bastion"]
}

manager = {
  flavor           = "s20.xlarge"    # 4CPU, 8Go RAM, 20Go
  name_prefix      = "node0"
  image            = "ubuntu-22.04"     # Nom de l'image OS des noeuds
}

node = {
  flavor           = "s10.medium"       # 2CPU, 2Go RAM, 10 Go
  name_prefix      = "node0"
  image            = "ubuntu-22.04"     # Nom de l'image OS des noeuds
  user             = "ubuntu"
  ssh_port         = 22
}

manager_role = [ "manager" ]
worker_role = [ "worker" ]
worker_count          = 1
manual_trigger        = 1
