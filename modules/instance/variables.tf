variable "name" {
  type = string
  description = "Nom de l'instance"
  default = "instance"
}

variable "image_name" {
  type = string
  description = "Image de l'instance"
  default = "imta-ubuntu22"
}

variable "flavor_name" {
  type = string
  description = "gabarit de l'instance"
  default = "m1.medium" # m1-medium : 2 cpu, 2Go ram, 5Go disque
}

variable "network_name" {
  type = string
  description = "Nom du réseau"
  default = "internal_network"
}

variable "secgroups" {
  type = list(string)
  description = "Groupes de sécurité de l'instance"
  default = [ "int_secgroup" ]
}

variable "floatip" {
  type = bool
  description = "Associer une IP flottante"
  default = false
}

variable "keypair_name" {
  type = string
  description = "Nom de la paire de clés SSH"
  default = "keypair_tf"
  validation {
      condition = length(var.keypair_name) > 2
      error_message = "Le nom de la paire de clés SSH doit contenir plus de 2 caractères"
  }
}

variable "external_network" {
  type = string
  description = "Pool des IP flottantes"
  default = "external"
}

variable "tags_role" {
  type = list(string)
  description = "Tags : rôle de l'instance"
  default = []
}