# Groupe de sécurité
resource "openstack_networking_secgroup_v2" "sg" {
  name = var.secgroup.name
  description = var.secgroup.description
}

resource "openstack_networking_secgroup_rule_v2" "sg_rules" {
  for_each = { for i, sg_rule in var.secgroup_rules: i => sg_rule }
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = each.value.protocol
  port_range_min    = each.value.port > 0 ? each.value.port : null
  port_range_max    = each.value.port > 0 ? each.value.port : null
  remote_ip_prefix  = each.value.source
  security_group_id = openstack_networking_secgroup_v2.sg.id

  depends_on = [
    openstack_networking_secgroup_v2.sg
  ]
}
