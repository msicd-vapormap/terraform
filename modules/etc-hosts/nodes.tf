resource "null_resource" "nodes_etc_hosts" {
  #count = length(var.nodes)
  for_each = { for node in var.nodes: node.name => node }
  
  triggers = {
    nodes_name      = join(",", var.nodes.*.name),
    node_ip         = join(",", var.nodes.*.access_ip_v4)
  }

  connection {
    host        = each.value.access_ip_v4
    type        = "ssh"
    port        = var.ssh_nodes.port
    user        = var.ssh_nodes.user
    private_key = file(var.ssh_nodes.keypair_path)
    agent       = "false"

    bastion_host        = var.ssh_bastion.floatip
    bastion_private_key = file(var.ssh_bastion.keypair_path)
    bastion_user        = var.ssh_bastion.user
    bastion_port        = var.ssh_bastion.port
  }

  provisioner "remote-exec" {
    inline = [<<EOT
            test -f /etc/hosts.INIT || cat /etc/hosts | sudo tee /etc/hosts.INIT
            rm -rf /tmp/hosts
            %{ for node in var.nodes ~}
                echo "${node.access_ip_v4} ${node.name}" >> /tmp/hosts
            %{ endfor ~}
            echo "$(cat /tmp/hosts; cat /etc/hosts.INIT)" | sudo tee /etc/hosts
            rm -rf /tmp/hosts
            EOT
    ]
  }
}