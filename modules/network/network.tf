resource "openstack_networking_network_v2" "int_network" {
    name           = var.int_network_name
    admin_state_up = true
}

resource "openstack_networking_subnet_v2" "int_subnet" {
    name           = var.int_subnet_name
    network_id     = openstack_networking_network_v2.int_network.id
    cidr           = var.int_subnet_cidr
    dns_nameservers = var.dns

    depends_on = [
        openstack_networking_network_v2.int_network
    ]
}