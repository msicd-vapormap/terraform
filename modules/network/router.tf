data "openstack_networking_network_v2" "ext_network" {
    name = var.ext_network_name
}

resource "openstack_networking_router_v2" "int_router" {
  name                = var.int_router_name
  admin_state_up      = true
  external_network_id = data.openstack_networking_network_v2.ext_network.id
}

resource "openstack_networking_router_interface_v2" "int_interface" {
    router_id  = openstack_networking_router_v2.int_router.id
    subnet_id  = openstack_networking_subnet_v2.int_subnet.id

    depends_on = [
        openstack_networking_router_v2.int_router,
        openstack_networking_subnet_v2.int_subnet
    ]
}