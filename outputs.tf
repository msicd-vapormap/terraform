output "bastion_floatip" {
  value = module.bastion.floatip
  description = "IP flottante de l'instance bastion"

  depends_on = [
    module.bastion
  ]
}

output "manager_floatip" {
  value = module.manager.floatip
  description = "IP flottante du manager node"

  depends_on = [
    module.manager
  ]
}

output "bastion_internal_ip" {
  value = module.bastion.access_ip_v4
  description = "IP interne de l'instance bastion"

  depends_on = [
    module.bastion
  ]
}

output "manager_internal_ip" {
  value = { 
    (module.manager.name) = module.manager.access_ip_v4
  }
  description = "IP interne des noeuds"

  depends_on = [
    module.manager
  ]
}

output "worker_internal_ip" {
  value = { for node in module.worker: node.name => node.access_ip_v4 }
  description = "IP interne des noeuds"

  depends_on = [
    module.worker
  ]
}
