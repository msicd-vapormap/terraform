# Déploiement de l'infrastructure as Code Terraform à partir d'un pipeline sur Gitlab

__Par__ : FETCHEPING FETCHEPING Rossif Borel  
__Email__ : rossifetcheping@outlook.fr  
__École__ : IMT Atlantique  
__Formation__ : Mastère spécialisé Infrastructures Cloud et DevOps  
__Année académique__ : 2022-2023  
__UE__ : Projet fil rouge  
__Dépôt Gitlab__ : <https://gitlab.imt-atlantique.fr/vapormap-cicd/terraform/>  
__Pages Gitlab__ : <https://vapormap-cicd.gitlab-pages.imt-atlantique.fr/terraform>  
__Date__ : 27 Février 2023  

## Prérequis

* Un compte Openstack avec un quota suffisant de ressources  
* Une paire de clé SSH existante sur Openstack  
* Un compte sur une instance Gitlab  
* Un token d'accès à l'api du projet avec des droits "maintainer" pour assurer la gestion du TF State par le Backend de Gitlab  

## Configurer l'infrastructure

* Modifier le fichier __main.auto.tfvars__ pour personnaliser les ressources de l'infrastructure à provisionner sur Openstack. Les valeurs existances sont fiables.  

## Configurer l'environnement du pipeline sur Gitlab

### Créer ou mettre à jour les variables Gitlab du projet "terraform"

* Se rendre dans l'onglet "Settings > CI/CD" du projet.  
* Dans la section "Variables", modifier/ajouter les variables suivantes:  
  * __TF_STATE_NAME__: nom du fichier TF State.  
  * __OPENRC_SH__: contenu du fichier OpenRC sans le password (Utiliser une variable de type "File")  
  * __OPENRC_PASS__: mot de passe Openstack  
  * __TOKEN_API_VAP__: token d'accès à l'API Gitlab du projet  

### Créer ou mettre à jour les variables Gitlab du groupe "vapormap-cicd"

* Se rendre dans l'onglet "Settings > CI/CD" du groupe.  
* Dans la section "Variables", modifier/ajouter les variables suivantes:  
  * __TF_PKG_NAME__: nom du package registry pour la publication des fichiers d'inventaire générés par Terraform.  
  * __TF_PKG_VERSION__: version du package registry
  * __PRIVATE_KEY__: contenu de la clé privée SSH (Utiliser une variable de type "File")  
  * __PRIVATE_KEY_NAME__: nom de la paire de clé SSH sur Openstack  
  * __TOKEN_API_GROUP__: token d'accès à l'API Gitlab du groupe avec les privilèges "owner"  
  * __BASTION_PUBLIC_IP__: adresse IP publique de l'instance bastion  
  * __NODE_PUBLIC_IP__: adresse IP publique du noeud manager  
  * __GROUP_ID__: Identifiant du groupe de projet "Vapormap"  

> Remarque: Pendant la création des variables __BASTION_PUBLIC_IP__ et __NODE_PUBLIC_IP__, il faut leur affecter des valeurs aléatoires. Elles seront mises à jour pendant l'exécution du pipeline une fois que Terraform les aura créées.  

## Déployer l'infrastructure sur Openstack

* Se rendre dans l'onglet "CI/CD > pipeline" du projet.  
* Cliquer sur le bouton "Run pipeline".  
* Sur la fénètre qui s'affiche, cliquer à nouveau sur le bouton "Run pipeline" pour valider l'opération sans renseigner de variables.  

## Détruire l'infrastructure construite sur Openstack

Pour détruire l'infrastructure, lancer manuellement le job destroy sur le pipeline précédemment déclenché  

## Résultats

Suite au déploiement, l'inventaire de l'infrastructure est généré dans le package registry du projet:  

* le fichier __hosts.ini__ contient l'inventaire de l'infrastructure qui sera exploité par Ansible  
* le fichier __public_ip.ini__ contient les noms et adresses IP flottantes associées aux instances Bastion et manager  
* le fichier __private_ip.ini__ contient les noms et adresse IP privées de tous les noeuds  

> Le TF State est systématiquement mis à jour sur le Backend du projet dans Gitlab  
